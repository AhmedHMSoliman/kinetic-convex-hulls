Project: Kinetic Convex Hulls 
========

Author: Ahmed Soliman
=======

What it does?
=============

Visualize the process of maintaining the convex hull of points moving along linear trajectories in the plane.



First we start with a set of n points. (randomly chosen in general position, i.e. no three points are collinear).

A timer event is to be generated periodically with short periods (say, every 2 seconds).

Each time a timer event occurs, the points are animated in the plane according to random displacements (to simulate moving along linear trajectories in the plane).

This is followed by convex hull computation for the points at their new locations (coordinates) only if a qualifying event occurs.

An event queue will be triggered if and only if the convex hull of the dynamic points needs to be recomputed.

The updated convex hull is shown by drawing the linesegments connecting its vertices on the graph with a different color.

The user will have the ability to determine the number of random points to start with, the timer periods, the maximum amount of displacement per step and restart the whole process with new points.
 

 Tools
=====

Javascript in a browser.
